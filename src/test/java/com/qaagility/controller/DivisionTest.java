package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import com.qaagility.controller.*;

public class DivisionTest {

    @Test
    public void testD() {
        Division cnt = new Division();
        int a = 10;
        int b = 2;
        int expected = a / b;
        assertEquals("The d() method did not return the expected result.", expected, cnt.division(a, b));
    }

    @Test
    public void testDWithZero() {
        Division cnt = new Division();
        int a = 10;
        int b = 0;
        int expected = Integer.MAX_VALUE;
        assertEquals("The d() method did not return the expected result when dividing by zero.", expected, cnt.division(a, b));
    }
}
