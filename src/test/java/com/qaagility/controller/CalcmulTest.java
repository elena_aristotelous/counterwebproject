package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import com.qaagility.controller.*;

public class CalcmulTest {

    @Test
    public void testMul() {
        Calcmul calcmul = new Calcmul();
        int expected = 18;
        assertEquals("The mul() method did not return the expected result.", expected, calcmul.mul());
    }
}